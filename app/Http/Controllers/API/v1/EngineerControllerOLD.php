<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class EngineerController extends Controller
{
    public function get_engineers(){
        $result=DB::select("SELECT * from daftar_karyawan");
        return $result;
    }
    public function get_data(Request $request){
        $result=DB::insert("INSERT into daftar_karyawan(id, nama, alamat) 
                        VALUES('{$request->id}', '{$request->nama}', '{$request->alamat}')");
        if($result)return "OK";
        return "gagal";
    }

    public function get_projects(){
        $jumlah_hari=[31,28,31,30,31,30,31,31,30,31,30,31];

        $project=DB::select("SELECT * from daftar_proyek");
        foreach ($project as $key => $value) {
            $deadline=explode('-',$value->deadline);
            $tahun_end=(int)$deadline[0];
            $bulan_end=(int)$deadline[1];
            $tanggal_end=(int)$deadline[2];

            $tahun=date("Y");
            $bulan=date("m");
            $tanggal=date("d");

            $sisa_hari=0;
            while($tahun<$tahun_end){
                if($tahun%4==0)$jumlah_hari[1]=29;
                else $jumlah_hari[1]=28;
                while($bulan<=12){
                    while($tanggal<=$jumlah_hari[$bulan-1]){
                        $sisa_hari++;
                        $tanggal++;
                    }
                    $tanggal=1;
                    $bulan++;
                }
                $bulan=1;
                $tahun++;
            }
            if($tahun%4==0)$jumlah_hari[1]=29;
            else $jumlah_hari[1]=28;
            while($bulan<$bulan_end){
                while($tanggal<=$jumlah_hari[$bulan-1]){
                    $sisa_hari++;
                    $tanggal++;
                }
                $tanggal=1;
                $bulan++;
            }
            while($tanggal<=$tanggal_end){
                $sisa_hari++;
                $tanggal++;
            }

            
            $project[$key]->sisa_hari=$sisa_hari;
        }
        return $project;

    }


    public function get_kelompok_tugas($project=null){
        $result=DB::select("SELECT * from daftar_kelompok_tugas where id_proyek='{$project}'");
        return $result;
    }

    public function get_daftar_tugas($project=null){
        $result=DB::select("SELECT * from daftar_tugas where id_proyek='{$project}'");
        return $result;
    }
    public function displayImage($filename){
        $path = storage_public('public/images' . $filename);
        if (!File::exists($path)) {
            abort(404);
        }
        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }
    public function countDate(){
        $result=DB::select("SELECT * from deadline");
        return $result;
    }

}