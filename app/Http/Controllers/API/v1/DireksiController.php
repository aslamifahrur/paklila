<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class DireksiController extends Controller
{
    public function get_tracking(Request $request){
        $tanggal=$request->tanggal;
        $result=DB::table('tracker')
            ->where('tanggal',$tanggal)
            ->get();
            // ->paginate(2);
        return $result;
    }
}