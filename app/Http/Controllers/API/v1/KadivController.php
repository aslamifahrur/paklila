<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class KadivController extends Controller
{
    public function get_engineers(){
        $result=DB::select("SELECT * from daftar_karyawan");
        return $result;
    }

    public function get_projects(){
        $result=DB::select("SELECT * from daftar_proyek");
        return $result;
    }

    public function get_kelompok_tugas($project=null){
        $result=DB::select("SELECT * from daftar_kelompok_tugas where id_proyek='{$project}'");
        return $result;
    }

    public function get_daftar_tugas($project=null){
        $result=DB::select("SELECT * from daftar_tugas where id_proyek='{$project}'");
        return $result;
    }
}