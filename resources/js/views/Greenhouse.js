/*!

=========================================================
* Black Dashboard React v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/black-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
import getGraph from "../variables/Call.js";
import DatePicker from "react-datepicker";
import TextField from '@material-ui/core/TextField';

import "react-datepicker/dist/react-datepicker.css";


// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components
import {
  chartExample1,
  chartExample2,
  chartExample3,
  chartExample4
} from "../variables/charts.js";

import Mbh from "./MbhFunc.js";

function dateToString(date=new Date()){
  const tahun=date.getFullYear();
  let bulan=date.getMonth()+1;
  let tanggal=date.getDate();

  if(bulan<10)bulan="0"+bulan;
  if(tanggal<10)tanggal='0'+tanggal;
  console.log(tahun+'-'+bulan+'-'+tanggal);
  return tahun+'-'+bulan+'-'+tanggal;
}

class Greenhouse extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bigChartData1: "harian",
      bigChartData2: "harian",

      data1: [],
      labels1: [],
      //selectedDate: new Date(),
      startDate: new Date(),
      data2: [],
      labels2: []
    };
  }

  handleChange = date => {
    this.setState({
      startDate: date
    });
    this.updateData1();
  };
  
  setBgChartData1 = harian => {
    this.setState({
      // bigChartData1: name,
      bigChartData1: harian
    }, this.updateData1());
  };
  setBgChartData2 = mingguan => {
    this.setState({
      // bigChartData1: name,
      bigChartData1: mingguan
    }, this.updateData2());
  };
  setBgChartData3 = bulanan => {
    this.setState({
      // bigChartData1: name,
      bigChartData2: bulanan
    }, this.updateData3());
  };

  componentDidMount() {
    this.updateData1()
    this.updateData2()
    this.updateData3()
  }

  updateData1 = () => {
    fetch("http://127.0.0.1:8000/api/v1/get_data?hari="+dateToString(this.state.startDate), {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then(result => result.json()) //dapat data dari result.jeson
      .then(anu => {
        this.setState({
          data1: anu.data,
          labels1: anu.label
        })
      }) //variabel setkaryawan diisi anu
      .catch(error => console.log(error))
  }

  updateData2 = () => {

    fetch("http://127.0.0.1:8000/api/v1/get_data?hari="+dateToString(this.state.startDate), {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then(result => result.json()) //dapat data dari result.jeson
      .then(anu => {
        this.setState({
          data2: anu.data2,
          labels2: anu.label
        })
      }) //variabel setkaryawan diisi anu
      .catch(error => console.log(error))
  }

  updateData3 = () => {
    fetch("http://127.0.0.1:8000/api/v1/get_data", {
      method: "GET",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    })
      .then(result => result.json()) //dapat data dari result.jeson
      .then(anu => setKaryawans(anu)) //variabel setkaryawan diisi anu
      .catch(error => console.log(error))

  }

  render() {

    return (
      <>
        <div className="content">
          <Row>


            <Col xs="12">

              <Card className="card-chart">
                <CardHeader>
                  <Row>
                    <Col className="text-left" sm="6">
                      <h5 className="card-category"></h5>
                      <CardTitle tag="h2">Temperature</CardTitle>
                    </Col>
                    <Col sm="6">

                      <ButtonGroup
                        className="btn-group-toggle float-right"
                        data-toggle="buttons"
                      >
                         <DatePicker
                          selected={this.state.startDate}
                          onChange={this.handleChange}
                        />
                        <TextField id="filled-basic" label="Set Point" variant="filled" color="primary" />
                       
                        {/* <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData1 === "harian"
                          })}
                          color="info"
                          id="0"
                          size="sm"
                          onClick={() => this.setBgChartData1("harian")}
                        >
                          <input
                            defaultChecked
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Harian
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-single-02" />
                          </span>
                        </Button> */}
                        {/* <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData2 === "mingguan"
                          })}
                          color="info"
                          id="1"
                          size="sm"
                          onClick={() => this.setBgChartData2("mingguan")}
                        >
                          <input
                            // defaultChecked
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            mingguan
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-single-02" />
                          </span>
                        </Button> */}

                        {/* <KeyboardDatePicker
                          disableToolbar
                          variant="inline"
                          format="MM/dd/yyyy"
                          margin="normal"
                          id="date-picker-inline"
                          label="Date picker inline"
                          value={selectedDate}
                          onChange={handleDateChange}
                          KeyboardButtonProps={{
                            'aria-label': 'change date',
                          }}
                        /> */}
                        {/* <Button
                          color="info"
                          id="2"
                          size="sm"
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData3 === "bulanan"
                          })}
                          onClick={() => this.setBgChartData3("bulanan")}
                        > */}
                          {/* <input
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Bulanan
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-gift-2" />
                          </span> */}
                        {/* </Button> */}
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  {/* <div className="chart-area">
                    <Line
                      // data={chartExample1[this.state.bigChartData1]}
                      // options={chartExample1.options}
                      data={data}
                      options={options}
                    />
                  </div> */}
                  <Mboh labels={this.state.labels1} data={this.state.data1} />
                </CardBody>
              </Card>
            </Col>
            <Col xs="12">
              <Card className="card-chart">
                <CardHeader>
                  <Row>
                    <Col className="text-left" sm="6">
                      <h5 className="card-category">Grafik Nilai Humidity</h5>
                      <CardTitle tag="h2">Humidity</CardTitle>
                    </Col>
                    <Col sm="6">
                      <ButtonGroup
                        className="btn-group-toggle float-right"
                        data-toggle="buttons"
                      >
                        {/* <Button
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData4 === "harian2"
                          })}
                          color="info"
                          id="3"
                          size="sm"
                          onClick={() => this.setBgChartData4("harian")}
                        >
                          <input
                            defaultChecked
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Harian
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-single-02" />
                          </span>
                        </Button>
                        <Button
                          color="info"
                          id="4"
                          size="sm"
                          tag="label"
                          className={classNames("btn-simple", {
                            active: this.state.bigChartData5 === "mingguan2"
                          })}
                          onClick={() => this.setBgChartData5("mingguan2")}
                        >
                          <input
                            className="d-none"
                            name="options"
                            type="radio"
                          />
                          <span className="d-none d-sm-block d-md-block d-lg-block d-xl-block">
                            Mingguan
                          </span>
                          <span className="d-block d-sm-none">
                            <i className="tim-icons icon-gift-2" />
                          </span>
                        </Button> */}
                      </ButtonGroup>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  {/* <div className="chart-area">
                    <Line
                      // data={data}
                      // options={options}
                    />
                  </div> */}
                  <Mbh labels={this.state.labels2} data={this.state.data2} />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Greenhouse;
